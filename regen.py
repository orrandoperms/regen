"""
First cut at parsing RUSA perms page.

The basic idea here is to:
   - Copy most of a previous version of the ORR Permanents web page --- so that
     any editing of style and content is preserved in new version
   - Generate one portion, the permanents table, from data scraped from the
     RUSA permanents database display.

M Young, 28 Nov 2013
"""

from html.parser import HTMLParser
import http.client, urllib.parse, ssl
import datetime
import logging
logging.basicConfig(level=logging.WARN)

class Parser(HTMLParser):

    def __init__(self, destfile):
        self.row = [ ]
        self.in_row = False
        self.col_text = ""
        self.href = ""
        self.destfile = destfile
        HTMLParser.__init__(self)

    def handle_starttag(self, tag, attrs):
        """
        Start tag --- the ones we care about are "tr"  (starting a table row),
        "td" (starting a column in the table), and "a"  (hyperlink).
        """  
        if tag=="tr":          # Table row, but we don't know if it's the right table
            self.row = [ ]     # We'll gather up the columns in case, and test later
            self.in_row = True
            self.href = ""
            logging.debug("Entering table row")
        elif tag=="td":        # Within each column, we gather up text
            self.col_text = ""
            logging.debug("Entering column")
        elif tag=="a":         # Links could be to RUSA permanent records
            self.href = attrs[0][1]

    def handle_data(self, data):
        """
        If we are in a row, we gather up any text encountered to go in the
        columns.  col_data is initialized to "" at "tr" tag, start of column,
        and stored into the rows variable at matching "/tr" end-tag.
        """
        if self.in_row:
            self.col_text += data

    def handle_endtag(self, tag):
        """
        At end of column (/td), place gathered text into next element of row.
        At end of row, only if it looks like one of the permanent records we
        are snarfing, process the whole row and transform into the data we
        want.
        """
        if tag=="td":
            self.row.append(self.col_text)
            self.col_text = ""
        if tag=="tr":
            logging.debug("Exiting row with values {}".format(self.row))
            if len(self.row) >= 8:
                states = self.row[7]
            else:
                states = ""
            if len(self.row) > 0  and "OR" in states: 
                logging.debug("Emitting")
                self.emit(self.row, self.href)
            else:
                logging.debug("Not emitting row: {}".format(self.row))
            self.in_row = False
            self.row = [ ]

    def emit(self, row, href):
        """
        Here is where we isolate knowledge of what we want the output
        html to look like, as well as order of columns in the original.
        """
        if len(row) != 8:
            logging.warning("Encountered bad row: {}".format(row))
        assert(len(row) >= 8) ## Or else I've made a bad assumption about input
        perm_loc, perm_fr, perm_km, perm_climb, perm_super, perm_name, perm_owner, perm_states = row
        logging.debug(("Decoding columns as loc: " +
                      "{loc}, km: {km}, climb: {climb}, super: {super}, " +
                      "name: {name}, owner: {owner}, states: {states}," +
                      " free-route: {free}").format(
                          loc=perm_loc, free=perm_fr, km=perm_km,
                          climb=perm_climb,  super=perm_super,
                          name=perm_name, owner=perm_owner, states=perm_states))

        # We are seeing \xA0 in some empty fields, including climbing feet
        # and owner.  Repair those here: 
        if not perm_climb.isdecimal():
            perm_climb=" "
        if not perm_owner.isprintable():
            perm_owner="Unassigned"


        # A couple of less common options show up in 'Notes' to make the
        # table more readable
        perm_notes = ""
        if perm_fr == "yes":
            perm_notes += "Free-Route "
        if perm_super == "yes":
            perm_notes += "Super Randonn&eacute;e"


        href= "http://www.rusa.org" + href
        table_line_skel="""<tr class="perm_row">
                 <td class="loc">{loc}</td>
                 <td class="km">{km}</td>
                 <td class="climb">{climb}</td>
                 <td class="name"><a href="{href}">{name}</a></td>
                 <td class="owner">{owner}</td>
                 <td class="notes">{notes}</td>
                 </tr>
                 """
        table_line = table_line_skel.format(loc=perm_loc, km=perm_km,
                            climb=perm_climb, href=href, name=perm_name,
                            owner=perm_owner, notes=perm_notes)
        print(table_line, file=self.destfile)

def stitch(template, rusa, destfile):
    copying = True
    replaced = False
    for line in template:
        if line.strip().startswith("""<span ID="timestamp">"""):
            print("""<span ID="timestamp">generated from RUSA data {}</span>""".format(
                datetime.date.today().isoformat()), file=destfile)
            continue
        if   "### INSERT HERE ###" in line:
            print(line.rstrip(), file=destfile)
            copying = False

            ### Insert table from rusa
            parser = Parser(destfile)
            #for line in rusa:
            parser.feed(rusa)
            parser.close()

        elif "### END INSERT ###" in line and not copying:
            copying = True
            replaced = True
            print(line.rstrip(), file=destfile)
        elif copying:
            print(line.rstrip(), file=destfile)

def snarf():
    """Suck down perms running through Oregon from RUSA, and return them
    as a big string.
    """
    params = urllib.parse.urlencode({'start': "", # All locations
                                 'dist': "",  # All distances
                                 'free': "",  # Free route or not
                                 'type': "", # all shapes
                                 'through': "OR", # perms through Oregon 
                                 'owner': "", # all owners
                                 'sortfield': "locaton", # sort by location (?Fix spelling)
                                 'showall': "", # show inactive routes
                                 'submit': "search"})
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain"}
    logging.debug("Creating HTTPSConnection")
    conn = http.client.HTTPSConnection("rusa.org",
                                       context=ssl._create_unverified_context()) # Insecure mode
    logging.debug("Created HTTPSConnection")
    conn.request("POST", "/cgi-bin/permsearch_PF.pl", params, headers)
    response = conn.getresponse()
    data = response.read()  # Returns a "bytes" object?
    as_str = str(data,encoding="UTF-8")
    logging.debug("Response from server: {}...".format(as_str[0:200]))
    return as_str

if __name__ == "__main__":
    import sys
    import argparse
    parser = argparse.ArgumentParser(description="Regenerate permanents page with fresh data.")
    parser.add_argument('template', help="Base new html file on this old file",
                         type=argparse.FileType('r'))
    parser.add_argument('newfile', help="Put the regenerated page here",
                        type=argparse.FileType('w'), default=sys.stdout)
    args = parser.parse_args()
    from_rusa = snarf()  #This is the raw html; parser is invoked in 'stitch'
    stitch(args.template, from_rusa, args.newfile)

                



