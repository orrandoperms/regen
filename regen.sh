#! /bin/sh
#
# Make a copy of ../permanents/index.html with fresh data from 
# RUSA;  replace the original and save it as old_index.html
# 
# This script runs on Michal's machines; it will need editing
# to run elsewhere. 
# 
PYTHON=python3   # Python 3.3.3 on Mac OS X is called 'python3';  substitute path for another environment

cp ../permanents/index.html . 
${PYTHON} regen.py index.html new.html
chmod a+r new.html
cp ../permanents/index.html ../permanents/old_index.html
mv new.html ../permanents/index.html
