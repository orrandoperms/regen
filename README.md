# README #

Regenerates OrRando perm page from RUSA database and template

### What is this repository for? ###

* Scrapes RUSA permanents page selecting perms in or passing through Oregon
* Incorporates boilerplate before and after the table itself

### What does it do? ###

* regen.sh expects to find OR permanents page template at ../permanents/index.html
* regen.py scrapes https://rusa.org/cgi-bin/permsearch_PF.pl to obtain data for table
* regenerated perms page overwrites ../permanents/index.html after copying to old_index.html

### Who do I talk to? ###

* Author: michal.young@gmail.com
* Webmaster: Michael Rasmussen, michael@jamhome.us
* Page design, usability: Lynne Fitzsimmons